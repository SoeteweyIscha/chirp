﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMoveScript : MonoBehaviour
{

    // Background scroll speed can be set in Inspector with slider
    [Range(0.1f, 5f)]
    [SerializeField] private float scrollSpeed = 1f;

    // Scroll offset value to smoothly repeat backgrounds movement
    private float scrollOffset;

    // Start position of background movement
    private Vector2 startPos;

    // Backgrounds new position
    private float newPos;

    // Use this for initialization
    void Start()
    {
        // Getting backgrounds start position
        startPos = transform.position;
        scrollOffset = transform.position.x * -1;
    }

    // Update is called once per frame
    void Update()
    {
        // Calculating new backgrounds position repeating it depending on scrollOffset
        newPos = Mathf.Repeat(Time.time * -scrollSpeed, scrollOffset);

        // Setting new position
        transform.position = startPos + Vector2.right * newPos;
    }
}
