﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteBehaviourScript : MonoBehaviour {

	// Use this for initialization


	public void playNote()
	{
		this.GetComponent<AudioSource>().Play();
		this.GetComponentInChildren<Animator>().SetTrigger("Sing");
	}

	// Update is called once per frame

}
