﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Api;
using UnityEngine;

public class ElectricityMove : MonoBehaviour
{
	
    [SerializeField] private Transform _startMarker;
    [SerializeField]  private Transform _endMarker;
    [SerializeField] private float _duration;
    private float _timer = 0;
    public bool AllCompleted;


    private void Update()
    {
        _timer += Time.deltaTime;
        float step = _timer/_duration;
        transform.position = Vector3.Lerp(_startMarker.position, _endMarker.position, step);


        if(step >= 1)
        {
            _timer = 0;

        }

    
	}
	
		private void OnTriggerEnter(Collider other)
	{
		//Debug.Log(other.name);
		if (other.tag == "Note")
		{
			other.GetComponent<NoteBehaviourScript>().playNote();

		    AllCompleted = true;
		}
	}
}
